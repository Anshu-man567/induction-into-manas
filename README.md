# Induction into Manas
While some of you might have completed ROS related tasks, it is still recommmended to you to go through the links below.
- [ROS1 Concepts](http://wiki.ros.org/ROS/Concepts)
- [RQT and ROS Launch](http://wiki.ros.org/ROS/Tutorials/UsingRqtconsoleRoslaunch)
- [RViz](http://wiki.ros.org/rviz/UserGuide)
- [TF](http://wiki.ros.org/tf)
- [ROS Bags](http://wiki.ros.org/ROS/Tutorials/Recording%20and%20playing%20back%20data) [and its ops](http://wiki.ros.org/ROS/Tutorials/reading%20msgs%20from%20a%20bag%20file) <!-- not really sure about this, or should we provide only the first link?? -->
- [ROS1 Navigation Stack](http://wiki.ros.org/navigation/Tutorials); go thorough the `Configuring and Using the Navigation Stack` and `Configuring and Using the Global Planner of the Navigation Stack` sections. 
- The last thing to do is to run the launch files, and observe how things work!! [Husky's Code Repo](https://github.com/husky/husky) and [Husky's NavStack Tutorials](http://wiki.ros.org/husky_navigation/Tutorials/) 
(PS: You may either build Husky's Code or use the already available binaries, could be found on the tutorials page)
